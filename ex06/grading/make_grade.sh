#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2021 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 10 \
#     --comment1 'Add a comment to justify your score' \ # optional
#     --comment1 'Add a comment if you had problems somewhere' \ # optional
#     --question2 10 \
#     --comment2 'Comment for question 2' \ # optional
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle:

python3 grade.py \
    --question1 8.75 \
	--comment1 '-1.25 for not catchign segmentation fault in a), -6 for getting c) wrong' \
    --question2 0 \
    --comment2 'Didnt do' \
    --question3 0 \
    --comment3 'Didnt do' \
