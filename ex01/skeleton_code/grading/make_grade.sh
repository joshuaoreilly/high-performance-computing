#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2021 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 0 \
#     --comment1 'Add a comment to justify your score' \
#     --question2 0 \
#     --question2 0 \
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle:
# https://moodle-app2.let.ethz.ch/course/view.php?id=13666

# Note: --question2 and --question5 are not graded
python3 grade.py \
	--question1 24 \
	--comment1 'Answers for a) match solution: 6pts' \
	--comment1 'Answers for b) are a close match, and question phrasing is incredibly ambiguous/opaque, so I gave myself half points: 6pts' \
	--comment1 'Answers for c) match solutions: 12pts' \
	--question2 30 \
	--comment2 'Answers for a) match solution: 10pts' \
	--comment2 'Answers for b) match solution: 12pts' \
	--comment2 'Answers for c) match solution: 8pts' \
	--question3 20 \
	--comment3 'My code for a) matches the solution. My explanation and results do not. In fact, the given explanation in the solution01.pdf document goes off on a tangent about vectorization (which is the next homework) and does not address cache at all. Im giving myself full points because my answer makes sense in the context of the homework, ie. cache hits and misses: 4pts' \
	--comment3 'Again, I *think* I have the correct implementation and explanation, but its hard to tell from difficult to undersand or draw relevance from explanations; it hardly helps that they use a different unit of measurement along the y=axis (multiplications per second) then we are given (execution time). Additionally, my results arent as clean as theirs, with my blocking version sometimes performing worse than the non-blocking one. Comparing with a peer indicates that this is normal, so Im not sure how the reference solution manages to get better performance on the blocking version for all block sizes and matrix sizes. That said, I did everything the scoring system is looking for, so full points: 6pts' \
	--comment3 'Again, my results look different, but my conclusion is the same and my code works: 10pts'
