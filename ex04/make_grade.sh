#!/usr/bin/env bash
# Copyright 2020 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 10 \ # my scored points
#     --comment1 'Add a comment to justify your score' \ # optional
#     --comment1 'Add a comment if you had problems somewhere' \ # optional
#     --question2 50 \ # my scored points
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle.

python grade.py \
    --question1 22 \
	--comment1 'All correct except for improper implementation of histogram (-3pts): 22/25'\
    --question2 15 \
	--comment2 'b) Proper bounds and N^2 iterations, but my code didnt use atomic, and has collapse(2), so race condition to update a given dphi. Fixable by only iterating over outser loop in my case: 5/5 for part b), but 0/?? for part d). c) explanations okay, found h: 5/5. e) plotted them, as well as gave an answer that matched the TAs (strong scaling matters more). I used the equations itself to justify my answer as oppose to the graph, but in hindsight, I could have used it. 5/5'
