#include "sanger.hpp"
#include <cblas.h>

SangersRule::SangersRule(int in_dims, int out_dims, int max_batch)
  :  inference_(new scalar[out_dims_]), Perceptron(in_dims, out_dims, max_batch)
{}

SangersRule::~SangersRule() {
    delete[] inference_;
}

void SangersRule::backward(const scalar* data, int batch_size) {
    
    // TO DO: Compute outputs and save in buffer outputs_
    forward(data, outputs_, batch_size);

    // TO DO: Compte first component of gradient similar to Hebb's rule
    // The fist component is: X^T * X * W = X^T * O
    // Save intermediate results to grad_
    // 
    // from hebbs rule, but an alternative approach to calculating it involves
    // using matrix-matrix multiplication to calculate all of O at once
    /*
    cblas_dgemv(CblasRowMajor, CblasTrans,
		    batch_size, in_dims,
		    1.0, data, in_dims, outputs_, 1,
		    0.0, grad_, 1);
		    */
	// since X is transposed, op(A) = X^T, and thus the rows and columns of X are inverted
	// when setting variables M, N and K, as well as LDA
    cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans,
		    in_dims_, out_dims_, batch_size,
		    1.0,
		    data, in_dims_,
		    outputs_, out_dims_,
		    0.0,
		    grad_, out_dims_);

    // Iterate over output dimensions
    for(int k = 0; k < out_dims_; ++k) {

        // TO DO: Compute inference I_k = [o_1^T ... o_k^T] * o_k
        // Save inference in inference_
	// we're only computing the inference over the first k columns of the output at a time
	// k + 1 because we index starting at 1 in sanger's rule, but 0 in C++
	// 
	// unlike gemm, the row and column size of matrix O do not vary in their arguments, regardless
	// of whether tranposing or not
	// since individual o_k are column vectors, we need to hop a row for every element we
	// need of o_k, thus the incx = out_dims. Since each o_k represents a column, to get to the
	// desired column, we do outputs_ + k
	// That's about it, repeat for all o_k
	cblas_dgemv(CblasRowMajor, CblasTrans,
			batch_size, k + 1,
			1.0,
			outputs_, out_dims_,
			outputs_ + k, out_dims_,
			0.0,
			inference_, 1);

        // TO DO: Refine the gradient using the inference
        // Subtract from the k-th column of grad_: [w_1 ... w_k] * I_k
	// Same process as above, but no transposing our matrix this time
	// -1 scaling to act like the subtraction
	// incx = 1 because I is a true vector, not a column taken from a matrix
	// beta = 1, since we're adding the result of X^T o_i
	// outputing to column k of grad, incy is number of columns to skip to next value in column
	cblas_dgemv(CblasRowMajor, CblasNoTrans,
			in_dims_, k + 1,
			-1.0,
			weights_, out_dims_,
			inference_, 1,
			1.0,
			grad_ + k, out_dims_);

    }

}
