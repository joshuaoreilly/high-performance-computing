#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2020 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 10 \ # my scored points
#     --comment1 'Add a comment to justify your score' \ # optional
#     --comment1 'Add a comment if you had problems somewhere' \ # optional
#     --question2 50 \ # my scored points
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle.

python grade.py \
    --question1 22 \
    --comment1 '-1 for using same seed for all threads in all three solutions (-3 total). Didnt use a struct, instead extended the array of generators and only used those which corresponded to the beginnning of a nwe cache line. Entirely forgot about question c), somehow, so -10' \
    --question2 10 \
    --question3 20 \
>>>>>>> e847e5b2c6ab95555d60be60576589a7449a5ecc
