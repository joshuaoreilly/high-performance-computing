#include <iostream>
#include <random>
#include <stdio.h>

int main()
{
	std::uniform_real_distribution<double> u[3];
	std::default_random_engine g[3];
	double* doublepointer = new double(3);
	int* intpointer = new int(3);
	for (size_t i = 0; i < 3; i++)
	{
		g[i].seed(0);
	}
	std::cout << sizeof(u[0]) << std::endl;
	std::cout << sizeof(g[0]) << std::endl;
	std::cout << sizeof(doublepointer) << std::endl;
	std::cout << sizeof(*doublepointer) << std::endl;
	std::cout << sizeof(intpointer) << std::endl;
	std::cout << sizeof(*intpointer) << std::endl;
	return 0;
}
