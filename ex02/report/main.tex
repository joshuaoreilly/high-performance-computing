\documentclass{article}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{listings}           % For code blocks
\usepackage{graphicx}
\usepackage{amsmath}

\title{Set 2 - Vectorization and IPL}
\author{Joshua O'Reilly}
\date{\today}

\begin{document}
	\maketitle
	
	\section{Question 1: Manual Vectorization of Reduction Operator}
	
	\subsection{b)}
	
	\subsubsection{i)}
	
	Without Thread-Level Parallelism (TLP), I was expecting a maximum speed up of $4\textrm{x}$ for 32-bit precision and $2\textrm{x}$ for 64-bit precision, since four 32-bit floats can fit into a 128-bit register, while two 64-bit doubles can fit into the same 128-bit register.
	With TLP, I would expect the ceiling for performance scaling to be virtually infinite, given a sufficiently large array to operate on; the only non-parallel operation is dividing the target array into chunks for each thread, starting the threads, collecting the partial results, and summing them.
	The brunt of the code, the actual addition operations, are performed in parallel.
	Without attempting to calculate the precise speedup using Amdahl's Law, I expect the speedup to be less than $(4n)\textrm{x}$ and $(2n)\textrm{x}$ for 32-bit and 64-bit precision respectively, where $n$ is the number of threads used.
	
	\subsubsection{ii)}
	
	\includegraphics[page=1,width=.45\textwidth]{speedup_4-way.pdf}
	\includegraphics[page=1,width=.45\textwidth]{speedup_2-way.pdf}
	
	From the charts (left: 4-way SIMD (float), right: 2-way SIMD (double)), we can confirm the phenomenon I suggested above: the ceiling for performance scaling is pretty darn high thanks to little non-parallel work.
	We can also note that large arrays lose their speedup quite significantly compared to small arrays when it comes to the number of threads used.
	Perhaps equally interesting is that, at a small number of threads, the speedup of matching array sizes is approximately equal for 2-way SIMD and 4-way SIMD.
	I will attempt to explain this before addressing the other phenomenon above.
	
	First, the operational intensity.
	In the case of 32-bit floats, for every 4 floats we bring into cache and add to the sum, we have 128 bits of memory operations (32 times 4) and 4 FLOPS, adding the four new floats to the partial sums.
	We then have four final FLOPS in order to sum up the partial sums, and a final memory write to save the result from cache to ram.
	In total:
	
	\begin{equation}
		\textrm{OI}_{32} = \frac{4N + 4}{128N + 1} \approx \frac{4}{128} = \frac{1}{32}
	\end{equation}
	
	Repeating this process for 64-bit doubles, we note that the same number of bits are pulled into vector registeres at a time (still 128-bits, since that's the size of the registers), but we're performing half the flops, effectively giving an IO of
	
	\begin{equation}
		\textrm{OI}_{64} = \frac{2N + 4}{128N + 1} \approx \frac{2}{128} = \frac{1}{64}
	\end{equation}
	
	That said, these results presume that the only data being pulled into cache is data which is immediately being fed into the vector registers.
	In reality, when 128 bits of values are grabbed from ram, in both the case of floats and doubles, the same number of additional bits of data are being pulled in, and so the number of operations per bit of data in cache is identical.
	Thus, why the performance between 2-way and 4-way SIMD is identical when other variables are accounted for.
	
	When we account for running in parallel, though, the reasoning for why large array sizes have worse speedup is not at all clear to me, or at least, not how OI fits into the explanation
	
	\section{Question 2: Intel SPMD Program Compiler (ISPC)}
	
	\subsection{d)}
	
	As per wikipedia, SSE2 operations on 128-bit vector registers, while AVX2 operates on 256-bit vector registers.
	This means that, hypothetically, code run using AVX2 could run twice as fast as SSE2.
	This is clearly not the case, as relative speedups for SSE2 and AVX2 of 1.68 and 1.86 respectively were obtained for doubles (64-bit float), both with 8-element block sizes (that which gave the highest possible speed up for one of the other).
	Relative speedups of 1.35 and 1.4 were obtained for floats (32-bit floats) with 16-element blocks, which gave the second highest speedup for AVVX2 (1.6 at 32-element blocks) and best for SSE2 (1.0 at 16-element blocks). 
	This could be due to the matrices being too large to fit into cache, and thus lots of time is wasted on memory access operations; extra vector register throughput means nothing if the data takes forever to get to that register.
	But honestly, I'm not sure.
	
	\subsection{e)}
	
	Error obtained was virtually zero, rounding error really, and a result of floating point calculations being an approximation on computers.
	
	\subsection{f)}
	
	No, if there were, maybe in the manner in which they represent floating point numbers?
	
	\section{Question 3: Lapack routines}
	
	\subsection{a)}
	
	Generally, a system of linear equations takes the form
	
	\begin{equation}
	Ax + b
	\end{equation}
	
	As per the slides for the exercise:
	
	\begin{equation}
	\begin{bmatrix}
	b_1	&	c_1	&	\cdots	&	\cdots	&	0 \\
	a_2	&	b_2	&	c_2	& 	\cdots	&	0 \\
	0	&	a_3	&	b_3	&	c_3	&	0 \\
	0	&	\cdots	&	\cdots	&	\cdots	&	0 \\
	0	&	\cdots	&	\cdots	&	a_N	&	b_N
	\end{bmatrix}
	\begin{bmatrix}
	x_1 \\
	x_2 \\
	x_3 \\
	\cdots \\
	x_N
	\end{bmatrix}
	=
	\begin{bmatrix}
	d_1 \\
	d_2 \\
	d_3 \\
	\cdots \\
	d_N
	\end{bmatrix}
	\end{equation}
	
	In the case of cubic splines we are searching for the second derivatives of the spline functions $f''$ as defined the the equation
	
	\begin{equation}
	f''_{i-1}\frac{\Delta_i}{6} + f''_i \frac{\Delta_i + \Delta_{i+1}}{3} + f''_{i+1} \frac{\Delta_{i+1}}{6} = \frac{y_{i+1} - y_i}{\Delta_{i+1}} - \frac{y_i - y_{i-1}}{\Delta_i}
	\end{equation}
	
	We can see that the elements $x_i$ correspond to $f''_i$, the elements of each row of matrix $A$ correspond to the $\Delta$ terms of our spline function; that is, $a_2 = \frac{\Delta_2}{6}$, $b_2 = \frac{\Delta_2+ \Delta_3}{3}$, and $c_2 = \frac{\Delta_3}{6}$.
	
	The elements of $b$ correspond to the right side of the spline equation; for example, $d_2 = \frac{y_3 - y_2}{\Delta_3} - \frac{y_2 - y_1}{\Delta_2}$.
	
	Vector $b$ is of dimensions $N \times 1$, where $N$ is the number of 2D points used to extrapolate the cubic spline.
	Matrix $A$ is of dimensions $N \times N$.
	Equally, we can say that $b$ and $A$ are of dimensions $n+1 \times 1$ and $n+1 \times n+1$ respectively, where $n$ is the number of piece-wise third-order polynomial functions.
	
	\subsection{b)}
	
	Never got around to actually coding the solution, oops.
	

\end{document}