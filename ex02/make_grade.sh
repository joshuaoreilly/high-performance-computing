#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2020 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 10 \ # my scored points
#     --comment1 'Add a comment to justify your score' \ # optional
#     --comment1 'Add a comment if you had problems somewhere' \ # optional
#     --question2 50 \ # my scored points
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle.

python grade.py \
    --question1 19 \
	--comment1 '-1 points for calculating OI using bits, not bytes, -2 for not identifying cache size and memory bound properties for last part' \
    --question2 36 \
	--comment2 'proper GEMM implementation, memory access optimized version of GEMM kernel (using blocking instead of loop unrolling as shown in the reference solution. Proper makefile modifications, faster and correct ISPC implementation. -4 for not addressing the expected speedups for SSE2 and AVX2. Mentioned machine precision (in other terms), -2 for f)' \
    --question3 3 \
	--comment3 'Only got three points for successfully identifying A and b, didnt do the rest'
